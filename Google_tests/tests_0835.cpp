//
// Created by anton on 27/10/2022.
//

#include "solution.h"
#include "gtest/gtest.h"

TEST(Challenge835, exampleOne){
    vector<vector<int>> img1{{1,1,0},{0,1,0},{0,1,0}};
    vector<vector<int>> img2{{0,0,0},{0,1,1},{0,0,1}};
    int expected = 3;

    ASSERT_EQ(Solution::largestOverlap(img1, img2), expected);
}

TEST(Challenge835, OnePixelPositive){
    vector<vector<int>> img1{{1}};
    vector<vector<int>> img2{{1}};
    int expected = 1;

    ASSERT_EQ(Solution::largestOverlap(img1, img2), expected);
}

TEST(Challenge835, OnePixelNull){
    vector<vector<int>> img1{{0}};
    vector<vector<int>> img2{{0}};
    int expected = 0;

    ASSERT_EQ(Solution::largestOverlap(img1, img2), expected);
}


TEST(Challenge835, LargeImageValueOne){
    vector<vector<int>> img1{{0,0,0,0,1},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}};
    vector<vector<int>> img2{{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{1,0,0,0,0}};
    int expected = 1;

    ASSERT_EQ(Solution::largestOverlap(img1, img2), expected);
}

