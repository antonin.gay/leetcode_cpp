//
// Created by anton on 17/10/2022.
//

/*
 * A pangram is a sentence where every letter of the English alphabet appears at least once.
 *
 * Given a string sentence containing only lowercase English letters, return true if sentence is a pangram,
 *  or false otherwise.
 */

#include "solution.h"

bool Solution::checkIfPangram(const std::string &sentence) {
/*
    We will use an array to remember if we've seen a letter already. On first meet, we add 1 to count.
    When count == 26 we return true.
*/

    int n_letters = 26;
    int count = 0;
    bool letter_seen[n_letters];
    fill_n(letter_seen, n_letters, false);

    for (char c: sentence) {
//        The char 'a' to 'z' have successive values, so 'a'-'a' = 0 and 'z'-'a' = 25
        if (letter_seen[c - 'a']) {
//            pass
        } else {
            count++;
            letter_seen[c - 'a'] = true;
            if (count == n_letters) {
                return true;
            }
        }
    }
    return false;
}
