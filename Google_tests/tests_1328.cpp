//
// Created by anton on 10/10/2022.
//


#include "gtest/gtest.h"
#include "solution.h"

TEST(C1328_PalindromeBreak, FirstExample) {
    string palindrome = "abccba";
    string expected = "aaccba";

    ASSERT_EQ(Solution::breakPalindrome(palindrome), expected);
}

TEST(C1328_PalindromeBreak, OneLetterExample) {
    string palindrome = "a";
    string expected; // = ""

    ASSERT_EQ(Solution::breakPalindrome(palindrome), expected);
}

TEST(C1328_PalindromeBreak, OnlyAsExample) {
    string palindrome = "aaa";
    string expected = "aab";

    ASSERT_EQ(Solution::breakPalindrome(palindrome), expected);
}

