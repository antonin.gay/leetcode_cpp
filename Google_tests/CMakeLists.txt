project(Google_tests)
add_subdirectory(lib)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})

# adding the Google_Tests_run target
add_executable(Google_Tests_run tests_0001.cpp utils_Test.cpp tests_0019.cpp tests_0658.cpp tests_1155.cpp tests_1578.cpp tests_1328.cpp tests_0334.cpp tests_0976.cpp tests_1832.cpp tests_0038.cpp tests_0692.cpp tests_0219.cpp tests_0835.cpp tests_0049.cpp tests_0223.cpp tests_0263.cpp)

# linking Google_Tests_run with leetcode_cpp which will be tested
target_link_libraries(Google_Tests_run src)

target_link_libraries(Google_Tests_run gtest gtest_main)
