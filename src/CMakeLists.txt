project(leetcode_cpp)

set(HEADER_FILES
        solution.h
        )

set(SOURCE_FILES
        c0001_TwoSum.cpp
        c0019_RemoveNodeList.cpp utils.cpp utils.h c0658_ClosestElements.cpp c1155_DiceSumTarget.cpp c1578_MinimumTimeColorful.cpp c0112_PathSum.cpp c0623_AddRawTree.cpp c1328_BreakPalyndrome.cpp c0334_IncreasingTriplet.cpp c0976_LargestPerimeterTriangle.cpp c2095_DeleteMiddleNode.cpp c1832_SentencePangram.cpp c0038_CountSay.cpp c0692_TopKFreqWords.cpp c0219_Duplicate2.cpp c0835_ImageOverlap.cpp c0049_GroupAnagrams.cpp c0374_GuessNumber.cpp c0223_RectanglesArea.cpp c0263_UglyNumber.cpp)

add_library(src STATIC ${SOURCE_FILES} ${HEADER_FILES})