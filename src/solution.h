//
// Created by anton on 27/09/2022.
//

#ifndef LEETCODE_CPP_SOLUTION_H
#define LEETCODE_CPP_SOLUTION_H

#include <vector>
#include <string>
#include "utils.h"

using namespace std;

class Solution {
public:
    static vector<int> twoSum(vector<int> &nums, int target);

public:
    static ListNode *removeNthFromEnd(ListNode *head, int n);

public:
    static vector<int> findClosestElements(vector<int> &arr, int k, int x);
//    https://leetcode.com/problems/find-k-closest-elements/

public:
    static int numRollsToTarget(int n, int k, int target);
//    https://leetcode.com/problems/number-of-dice-rolls-with-target-sum/

public:
    static int minCost(const string &colors, vector<int> &neededTime);
//    https://leetcode.com/problems/minimum-time-to-make-rope-colorful/

public:
    static bool hasPathSum(TreeNode *root, int targetSum);
//    https://leetcode.com/problems/path-sum/

public:
    static TreeNode *addOneRow(TreeNode *root, int val, int depth);
//    https://leetcode.com/problems/add-one-row-to-tree/

public:
    static string breakPalindrome(string palindrome);
//    https://leetcode.com/problems/break-a-palindrome/

public:
    static bool increasingTriplet(vector<int> &nums);
//    https://leetcode.com/problems/increasing-triplet-subsequence/

public:
    static int largestPerimeter(vector<int> &nums);
//    https://leetcode.com/problems/largest-perimeter-triangle/

public:
    static ListNode *deleteMiddle(ListNode *head);
//    https://leetcode.com/problems/delete-the-middle-node-of-a-linked-list/

public:
    static bool checkIfPangram(const string &sentence);
//    https://leetcode.com/problems/check-if-the-sentence-is-pangram/

public:
    static string countAndSay(int n);

//    https://leetcode.com/problems/count-and-say/
    static string say(const string& str);

    static void updateString(int prev_digit, int *count, string *result);

public:
    static vector<string> topKFrequent(vector<string>& words, int k);
//    https://leetcode.com/problems/top-k-frequent-words/

public:
    static bool containsNearbyDuplicate(vector<int>& nums, int k);
//    https://leetcode.com/problems/contains-duplicate-ii/

public:
    static int largestOverlap(vector<vector<int>>& img1, vector<vector<int>>& img2);
//    https://leetcode.com/problems/image-overlap/

public:
    static vector<vector<string>> groupAnagrams(vector<string>& strs);
//    https://leetcode.com/problems/group-anagrams/

public:
    static int guessNumber(int n);
//    https://leetcode.com/problems/guess-number-higher-or-lower/

public:
    static int computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2);

public:
    static bool isUgly(int n);
//    https://leetcode.com/problems/ugly-number/
};

#endif //LEETCODE_CPP_SOLUTION_H

