//
// Created by anton on 18/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"

TEST(C0038_CountSay, One) {
    int n = 1;
    string expected = "1";

    ASSERT_EQ(Solution::countAndSay(n), expected);
}


TEST(C0038_CountSay, Four) {
    int n = 4;
    string expected = "1211";

    ASSERT_EQ(Solution::countAndSay(n), expected);
}
