//
// Created by anton on 28/09/2022.
//

#include "gtest/gtest.h"
#include "utils.h"


TEST(UtilsTestSuite, ListNodeCreation) {
    vector<int> originalList{1, 2, 3, 4, 5};
    ListNode *node = createLinkedList(originalList);

    for (int &i: originalList) {
        ASSERT_EQ(i, node->val);
        node = node->next;
    }

}

TEST(UtilsTestSuite, ListNodeToVector) {
    vector<int> originalList{1, 2, 3, 4, 5};
    ListNode *node = createLinkedList(originalList);
    vector<int> newList = LinkedListToVector(node);

    EXPECT_EQ(originalList.size(), newList.size()) << "Lists are of unequal length";
    for (int i = 0; i < originalList.size(); i++) {
        EXPECT_EQ(originalList[i], newList[i]) << "Vectors x and y differ at index " << i;
        EXPECT_EQ(originalList[i], node->val);
        node = node->next;
    }

}
