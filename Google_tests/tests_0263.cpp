//
// Created by anton on 18/11/2022.
//


#include "solution.h"
#include "gtest/gtest.h"

TEST(Challenge263_Ugly, Example6){
    int n = 6;
    bool expected = true;

    ASSERT_EQ(Solution::isUgly(n), expected);
}

TEST(Challenge263_Ugly, Example1){
    int n = 1;
    bool expected = true;

    ASSERT_EQ(Solution::isUgly(n), expected);
}

TEST(Challenge263_Ugly, Example14){
    int n = 14;
    bool expected = false;

    ASSERT_EQ(Solution::isUgly(n), expected);
}

TEST(Challenge263_Ugly, Example0){
    int n = 0;
    bool expected = false;

    ASSERT_EQ(Solution::isUgly(n), expected);
}

