//
// Created by anton on 28/09/2022.
//

#include "solution.h"
#include "utils.h"

int recursive(ListNode *node, int n, int current_idx) {
    int len;
    if (node->next) {
        len = recursive(node->next, n, current_idx + 1);
    } else {
        len = current_idx + 1;
    }
    if (len - n - 1 == current_idx) {
//       If len=10 and n=2, we shall remove the node idx=8 (the last one being 9).
//       So when idx=7=len-n-1, we redefine the next node as the next's next
        node->next = node->next->next;
    }

    return len;
}

ListNode *Solution::removeNthFromEnd(ListNode *head, int n) {
    int len = recursive(head, n, 0);
    if (len == n) {
//        If we remove the first element, the recursive function will not do it
        return head->next;
    }

    return head;
}