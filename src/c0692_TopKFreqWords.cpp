//
// Created by anton on 19/10/2022.
//

/*
 * Given an array of strings words and an integer k, return the k most frequent strings.
 *
 * Return the answer sorted by the frequency from highest to lowest.
 * Sort the words with the same frequency by their lexicographical order.
 */

#include <map>
#include <algorithm>
#include "solution.h"

// source of the following: https://www.geeksforgeeks.org/sorting-a-map-by-value-in-c-stl/
// Comparator function to sort pairs according to second value
bool cmp(pair<string, int> &a, pair<string, int> &b) {
//    Sorting bigger number first, then by lexicographical order
    if (a.second == b.second) {
        return a.first < b.first;
    }
    return a.second > b.second;
}

// Function to sort the map according to value in a (key-value) pairs
vector<pair<string, int>> sort_map(map<string, int> &M) {
    // Declare vector of pairs
    vector<pair<string, int> > pair_vector;

    // Copy key-value pair from Map to vector of pairs
    for (auto &it: M) {
        pair_vector.emplace_back(it);
    }

    // Sort using comparator function
    sort(pair_vector.begin(), pair_vector.end(), cmp);

    return pair_vector;
}

vector<string> Solution::topKFrequent(vector<std::string> &words, int k) {
//    We fill a map as map['word'] = count
    map<string, int> word_count;

    for (const string &word: words) {
        word_count[word]++;
    }

    vector<pair<string, int>> sorted_vector = sort_map(word_count);

    vector<string> result;
    for (int i = 0; i < k; i++) {
        result.push_back(sorted_vector[i].first);
    }

    return result;
}
