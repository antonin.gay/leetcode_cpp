//
// Created by anton on 28/09/2022.
//

#include "gtest/gtest.h"
#include "solution.h"
#include "utils.h"


TEST(C0019_RemoveNodeListTestSuite, ExampleList) {
    const vector<int> &vector1 = vector<int>{1, 2, 3, 4, 5};
    ListNode *list = createLinkedList(vector1);
    int n = 2;
    vector<int> expected{1, 2, 3, 5};

    ListNode *resNode = Solution::removeNthFromEnd(list, n);
    vector<int> resVector = LinkedListToVector(resNode);

    EXPECT_EQ(resVector.size(), expected.size()) << "Lists are of unequal length";
    for (int &expectedVal: expected) {
        EXPECT_TRUE(resNode != nullptr);
        EXPECT_EQ(expectedVal, resNode->val);
        resNode = resNode->next;
    }
}

TEST(C0019_RemoveNodeListTestSuite, EmptiedList) {
    const vector<int> &vector1 = vector<int>{1};
    ListNode *list = createLinkedList(vector1);
    int n = 1;
    vector<int> expected{};

    ListNode *resNode = Solution::removeNthFromEnd(list, n);
    vector<int> resVector = LinkedListToVector(resNode);

    EXPECT_EQ(resVector.size(), expected.size()) << "Lists are of unequal length";
    for (int &expectedVal: expected) {
        EXPECT_TRUE(resNode != nullptr);
        EXPECT_EQ(expectedVal, resNode->val);
        resNode = resNode->next;
    }
}