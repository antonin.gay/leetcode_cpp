//
// Created by anton on 11/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"

TEST(C0334_IncreasingTriplet, SortedExample) {
    vector<int> nums{1, 2, 3, 4, 5};
    bool expected = true;

    ASSERT_EQ(Solution::increasingTriplet(nums), expected);
}

TEST(C0334_IncreasingTriplet, InverseSortedExample) {
    vector<int> nums{5, 4, 3, 2, 1};
    bool expected = false;

    ASSERT_EQ(Solution::increasingTriplet(nums), expected);
}


TEST(C0334_IncreasingTriplet, NotsortedExample) {
    vector<int> nums{2, 1, 5, 0, 4, 6};
    bool expected = true;

    ASSERT_EQ(Solution::increasingTriplet(nums), expected);
}


TEST(C0334_IncreasingTriplet, DuplicateNumberExample) {
    vector<int> nums{5, 4, 3, 1, 2, 2, 2, 1};
    bool expected = false;

    ASSERT_EQ(Solution::increasingTriplet(nums), expected);
}

TEST(C0334_IncreasingTriplet, NegativeNumbers) {
    vector<int> nums{2, 4, -2, -3};
    bool expected = false;

    ASSERT_EQ(Solution::increasingTriplet(nums), expected);
}

