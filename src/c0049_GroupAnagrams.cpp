//
// Created by anton on 28/10/2022.
//

/*
 * Given an array of strings strs, group the anagrams together. You can return the answer in any order.
 *
 * An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
 * typically using all the original letters exactly once.
 */


#include <algorithm>
#include <utility>
#include "solution.h"

struct pair_str_letters {
    string s;
    vector<char> letters;

    pair_str_letters(string s, const vector<char> &letters) : s(std::move(s)), letters(letters) {}
};

bool compareLetterCounts(pair_str_letters &pair1, pair_str_letters &pair2) {
//    A letter count 1 is inferior if the first different letter (alphabetical order) is less counted than in 2

    vector<char> &letters1 = pair1.letters;
    vector<char> &letters2 = pair2.letters;

    for (int i = 0; i < 26; i++) {
        if (letters1[i] < letters2[i]) return true; // true means 1 goes before 2
        if (letters1[i] > letters2[i]) return false; // true means 1 goes before 2
    }

    return true;
}

vector<vector<string>> Solution::groupAnagrams(vector<std::string> &strs) {
    vector<pair_str_letters> letter_counts;
    letter_counts.reserve(strs.size());

    for (string &s: strs) {
        vector<char> letters(26, 0);

        for (char &c: s) {
            letters[c - 'a']++;
        }
        letter_counts.emplace_back(pair_str_letters(s, letters));
    }

    sort(letter_counts.begin(), letter_counts.end(), compareLetterCounts);

    vector<vector<string>> result;
    vector<char> prev_letter(26, -1);

    for (auto &pair: letter_counts) {

        if (prev_letter == pair.letters) {
            result.back().push_back(pair.s);
        } else {
            result.push_back(vector<string>{pair.s});
            prev_letter = pair.letters;
        }
    }

    return result;

}
