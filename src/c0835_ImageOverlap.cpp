//
// Created by anton on 27/10/2022.
//

/*
 * You are given two images, img1 and img2, represented as binary, square matrices of size n x n.
 *  A binary matrix has only 0s and 1s as values.
 *
 *  We translate one image however we choose by sliding all the 1 bits left, right, up, and/or down any number of units.
 *   We then place it on top of the other image. We can then calculate the overlap by counting the number of positions
 *   that have a 1 in both images
 *
 *  Note also that a translation does not include any kind of rotation. Any 1 bits that are translated outside of the
 *   matrix borders are erased.
 *
 *  Return the largest possible overlap.
 */

#include "solution.h"

int Solution::largestOverlap(vector<vector<int>> &img1, vector<vector<int>> &img2) {

    int n = (int) img1.size();   // 1 <= n <= 30
    int largest_overlap = 0;

//    We go through all possible x / y shifts
    for (int x_shift = -n + 1; x_shift < n; x_shift++)
        for (int y_shift = -n + 1; y_shift < n; y_shift++) {

            int overlap = 0;

//            We go through all overlapping pixels to sum
            for (int x = 0; x < n - abs(x_shift); x++)
                for (int y = 0; y < n - abs(y_shift); y++) {

                    int x1 = x - min(x_shift, 0);
                    int x2 = x + max(x_shift, 0);
                    
                    int y1 = y - min(y_shift, 0);
                    int y2 = y + max(y_shift, 0);
                    
                    overlap += img1[x1][y1] * img2[x2][y2];
                }

            largest_overlap = max(largest_overlap, overlap);
        }

    return largest_overlap;
}
