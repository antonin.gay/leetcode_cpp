//
// Created by anton on 05/10/2022.
//

#include "solution.h"

TreeNode *Solution::addOneRow(TreeNode *root, int val, int depth) {
    if (root != nullptr) {
        if (depth == 1) {
            return new TreeNode(val, root, nullptr);
        } else if (depth == 2) {
//        If depth is 2, this mean the new row mus tbe inserted in the next row
            root->left = new TreeNode(val, root->left, nullptr);
            root->right = new TreeNode(val, nullptr, root->right);
        } else {
//        If the depth is deeper, we add the row to the two subtrees with depth = depth - 1
            Solution::addOneRow(root->left, val, depth - 1);
            Solution::addOneRow(root->right, val, depth - 1);
        }
    }
    return root;
}