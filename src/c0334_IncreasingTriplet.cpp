//
// Created by anton on 11/10/2022.
//

/*
 * Given an integer array nums, return true if there exists a triple of indices (i, j, k) such that
 * i < j < k and nums[i] < nums[j] < nums[k]. If no such indices exists, return false.
 *
 */

#include "solution.h"

bool Solution::increasingTriplet(vector<int> &nums) {
    vector<int> max_after(nums.size());
//    We fill a vector with the max value after a given index, y filling it from the emd.

    int max_val = nums[nums.size() - 1];
    for (int i = (int) max_after.size() - 2; i >= 0; i--) {
        max_after[i] = max_val;
        max_val = max(max_val, nums[i]);
    }

//    We remember the min value met before the index i
    int min_before = nums[0];
    for (int i = 1; i < nums.size(); i++) {
        int value = nums[i];
        if (min_before < value and value < max_after[i]) {
            return true;
        }
        min_before = min(min_before, value);
    }
    return false;
}

