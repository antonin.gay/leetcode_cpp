//
// Created by anton on 03/10/2022.
//

#include <string>
#include "solution.h"


int Solution::minCost(const string &colors, vector<int> &neededTime) {
//    We go through the string. We compare the current char with the previous one to know if we're in a chain (of
//    similar letters). At each letter, we add its time value to a sum, and compare it to the max time of the current
//    chain. At the end of a chain (changing letter) we remove from the count the stored max, which corresponds to the
//    balloon we leave.
    int total_time = 0;
    int current_chain_max = 0;
    char prev_char = '_';

    int i = 0;
    for (char c: colors) {
        total_time += neededTime[i];

        if (c == prev_char) {
//            We are still in the chain, so we update the max
            current_chain_max = max(current_chain_max, neededTime[i]);
        } else {
//            We just left the previous chain, so we reset the max to the current value and remove prev chain max
            total_time -= current_chain_max;
            current_chain_max = neededTime[i];
        }

        prev_char = c;
        i++;
    }

//    Finally, we remove the max of the last chain
    total_time -= current_chain_max;

    return total_time;
}