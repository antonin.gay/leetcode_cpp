//
// Created by anton on 21/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"

TEST(Challenge219, ExampleOne) {
    vector<int> nums{1, 2, 3, 1};
    int k = 3;
    bool expected = true;

    ASSERT_EQ(Solution::containsNearbyDuplicate(nums, k), expected);
}

TEST(Challenge219, ExampleTwo) {
    vector<int> nums{1, 0, 1, 1};
    int k = 1;
    bool expected = true;

    ASSERT_EQ(Solution::containsNearbyDuplicate(nums, k), expected);
}

TEST(Challenge219, ExampleThree) {
    vector<int> nums{1,2,3,1,2,3};
    int k = 2;
    bool expected = false;

    ASSERT_EQ(Solution::containsNearbyDuplicate(nums, k), expected);
}

