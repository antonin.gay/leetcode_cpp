//
// Created by anton on 02/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"
#include <cmath>

TEST(C1155_DiceSumTarget, OneDice) {
    int n = 1;
    int k = 6;
    int target = 3;
    int expected = 1;

    ASSERT_EQ(Solution::numRollsToTarget(n, k, target), expected);
}

TEST(C1155_DiceSumTarget, TwoDice) {
    int n = 2;
    int k = 6;
    int target = 7;
    int expected = 6;

    ASSERT_EQ(Solution::numRollsToTarget(n, k, target), expected);
}

TEST(C1155_DiceSumTarget, ThirtyDices) {
    int n = 30;
    int k = 30;
    int target = 500;
    int expected = 222616187 % int(pow(10, 9) + 7);

    ASSERT_EQ(Solution::numRollsToTarget(n, k, target), expected);
}


