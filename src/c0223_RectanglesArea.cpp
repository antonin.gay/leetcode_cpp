//
// Created by anton on 17/11/2022.
//

/*

Given the coordinates of two rectilinear rectangles in a 2D plane, return the total area covered by the two rectangles.

The first rectangle is defined by its bottom-left corner (ax1, ay1) and its top-right corner (ax2, ay2).

The second rectangle is defined by its bottom-left corner (bx1, by1) and its top-right corner (bx2, by2).


 */

#include "solution.h"

int Solution::computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2) {
    int common_area;

//    The length of the common rectangle among x-axis
    int shared_x = min(ax2, bx2) - max(ax1, bx1); 
    int shared_y = min(ay2, by2) - max(ay1, by1);
    
    if (shared_x <0 or shared_y < 0){
        common_area = 0;
    } else{
        common_area = shared_x * shared_y;
    }

    int area_a = (ax2 - ax1) * (ay2 - ay1);
    int area_b = (bx2 - bx1) * (by2 - by1);

    return area_a + area_b - common_area;
    
}