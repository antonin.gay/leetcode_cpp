//
// Created by anton on 10/10/2022.
//

/*
Given a palindromic string of lowercase English letters palindrome, replace exactly one character with any lowercase
 English letter so that the resulting string is not a palindrome and that it is the lexicographically smallest one
 possible.

Return the resulting string. If there is no way to replace a character to make it not a palindrome,
 return an empty string.

A string a is lexicographically smaller than a string b (of the same length) if in the first position where a and b
 differ, a has a character strictly smaller than the corresponding character in b.
 For example, "abcc" is lexicographically smaller than "abcd" because the first position they differ is at the fourth
 character, and 'c' is smaller than 'd'.
*/




#include "solution.h"

string Solution::breakPalindrome(string palindrome) {
    //  If there is only one letter, the palindrome cannot be broken
    if (palindrome.size() <= 1) {
        return "";
    }

    //  Otherwise, we go through the first half of the palindrome and replace the first letter that is not 'a' by 'a', which
    //   breaks the palindrome and creates the smallest word
    for (int i = 0; i < palindrome.size() / 2; i++) {
        char c = palindrome[i];
        if (c != 'a') {
            palindrome[i] = 'a';
            return palindrome;
        }
    }

    //  Finally, if we arrive here, it means the string is more than 1 letter long and contains only 'a'.
    //  Therefore, the smallest breaking consists in replacing the first letter by 'b'
    palindrome[palindrome.size() - 1] = 'b';
    return palindrome;
};