//
// Created by anton on 17/11/2022.
//

#include "solution.h"
#include "gtest/gtest.h"

TEST(Challenge223, One) {
    int ax1 = -3;
    int ay1 = 0;
    int ax2 = 3;
    int ay2 = 4;
    int bx1 = 0;
    int by1 = -1;
    int bx2 = 9;
    int by2 = 2;
    int expected = 45;

    ASSERT_EQ(Solution::computeArea(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2), expected);
}

TEST(Challenge223, Two) {
    int ax1 = -2;
    int ay1 = -2;
    int ax2 = 2;
    int ay2 = 2;
    int bx1 = -2;
    int by1 = -2;
    int bx2 = 2;
    int by2 = 2;
    int expected = 16;

    ASSERT_EQ(Solution::computeArea(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2), expected);
}

