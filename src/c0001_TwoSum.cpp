//
// Created by anton on 27/09/2022.
//

#include "solution.h"

vector<int> Solution::twoSum(vector<int> &nums, int target) {
    //todo: optimize by sorting the numbers (similar solution to Python)
    for (int i = 0; i < nums.size(); i++) {
        for (int j = 0; j < nums.size(); j++) {
            if (nums[i] + nums[j] == target && i != j) {
                vector<int> ret{i, j};
                return ret;
            }
        }
    }
    return vector<int>{0, 1};
}