//
// Created by anton on 18/10/2022.
//

#include "solution.h"

string Solution::countAndSay(int n) {
    string result = "1";

    for(int i = 1; i<n; i++){
        result = say(result);
    }

    return result;
}

string Solution::say(const std::string& str) {
    int prev_digit = -1;
    int digit;
    int count = 0;
    string result;

    for (char c: str) {
        digit = c - '0';  // '1' = 1, etc.

        if (digit != prev_digit) {
            updateString(prev_digit, &count, &result);
        }

        prev_digit = digit;
        count++;
    }
    updateString(prev_digit, &count, &result);

    return result;
}


void Solution::updateString(int prev_digit, int *count, string *result) {
    if (*count == 0) { return; }
    *result += to_string(*count) + to_string(prev_digit);
    *count = 0;
}
