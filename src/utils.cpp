//
// Created by anton on 28/09/2022.
//

#include "solution.h"
#include "utils.h"

ListNode *createLinkedList(vector<int> list) {
    ListNode *node = nullptr;
    for (unsigned long long i = list.size() - 1; i != -1; i--) {
        node = new ListNode(list[i], node);
    }
    return node;
}

vector<int> LinkedListToVector(ListNode *node) {
    vector<int> res{};
    while (node != nullptr) {
        res.push_back(node->val);
        node = node->next;
    }

    return res;

}