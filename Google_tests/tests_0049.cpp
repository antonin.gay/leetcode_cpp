//
// Created by anton on 28/10/2022.
//

#include "solution.h"
#include "gtest/gtest.h"


TEST(Challenge49, LongListExample) {
    vector<string> strs{"eat", "tea", "tan", "ate", "nat", "bat"};
//    Careful : order has no importance...
    vector<vector<string>> expected{{"nat", "tan"},
                                    {"ate", "tea", "eat"},
                                    {"bat"}};

    ASSERT_EQ(Solution::groupAnagrams(strs), expected);
}


TEST(Challenge49, EmptyList) {
    vector<string> strs{""};
//    Careful : order has no importance...
    vector<vector<string>> expected{{""}};

    ASSERT_EQ(Solution::groupAnagrams(strs), expected);
}


TEST(Challenge49, SingleStr) {
    vector<string> strs{"a"};
//    Careful : order has no importance...
    vector<vector<string>> expected{{"a"}};

    ASSERT_EQ(Solution::groupAnagrams(strs), expected);
}


