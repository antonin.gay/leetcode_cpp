//
// Created by anton on 21/10/2022.
//

/*
 * Given an integer array nums and an integer k, return true if there are two distinct
 * indices i and j in the array such that nums[i] == nums[j] and abs(i - j) <= k.
 *
*/

#include <map>
#include "solution.h"

bool Solution::containsNearbyDuplicate(vector<int> &nums, int k) {
    map<int, int> num_last_index;

    for (int i = 0; i < nums.size(); i++) {
//        We return true if the current num is marked true in the map of the k-previous numbers
        int num = nums[i];
        if (num_last_index.count(num)) {
            if (i - num_last_index[num] <= k) {
                return true;
            }
        }
//        We update the map by adding the current num and removing the k-th previous
        num_last_index[num] = i;
    }

    return false;
}
