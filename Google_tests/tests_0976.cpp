//
// Created by anton on 12/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"

TEST(C0976_PerimeterTriangle, SimpleExample) {
    vector<int> nums{2, 1, 2};
    int expected = 5;

    ASSERT_EQ(Solution::largestPerimeter(nums), expected);
}

TEST(C0976_PerimeterTriangle, ZeroExample) {
    vector<int> nums{1, 2, 1};
    int expected = 0;

    ASSERT_EQ(Solution::largestPerimeter(nums), expected);
}

