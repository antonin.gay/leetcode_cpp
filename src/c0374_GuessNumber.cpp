//
// Created by anton on 16/11/2022.
//

//We are playing the Guess Game. The game is as follows:
//
//I pick a number from 1 to n. You have to guess which number I picked.
//
//Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.
//
//You call a pre-defined API int guess(int num), which returns three possible results:
//
//-1: Your guess is higher than the number I picked (i.e. num > pick).
//1: Your guess is lower than the number I picked (i.e. num < pick).
//0: your guess is equal to the number I picked (i.e. num == pick).
//Return the number that I picked.

#include "solution.h"

int guess(int num);

int Solution::guessNumber(int n) {
    int begin = 0;
    int end = n;

    while (true) {
        if (end - begin == 1) {
            if (guess(begin) == 0) return begin;
            return end;
        }

        int num = (int) (((unsigned int) end + (unsigned int) begin) / 2);
        switch (guess(num)) {
            case -1:
                end = num;
                break;

            case 1:
                begin = num;
                break;

            case 0:
                return num;
        }
    }
}
