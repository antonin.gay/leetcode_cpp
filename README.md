# Leet Code challenges - C++

_A project by Antonin Gay_


## Description

[LeetCode](https://leetcode.com/) is a website proposing programming challenges to prepare for tech interviews. As a beginning for my upcoming work search, I started completing those challenges, firstly in Python [here](https://gitlab.com/antonin.gay/leetcode_challenges). However, I'm looking for new skills in my next job, therefore I decided to anticipate this and started completing the challenges in C++ too, a language I used a bit few years ago, and which I'd like to use more in future works.

This projects contains all my code for completing LeetCode challenges. I'm focusing on the daily challenges as well as the [top interview questions collection](https://leetcode.com/problem-list/top-interview-questions/).


## Support

If you're passing by and want to give me advices on the code, I'll take them with great pleasure :D ! I'm eager to improve and any help or advice on the code, its optimization, its organization or the way to code would be great!

[//]: # (todo: add badges, such as tests passed)

