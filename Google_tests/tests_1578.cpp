//
// Created by anton on 03/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"

TEST(C1578_MinimumTimeColorful, SimpleTest) {
    string colors = "abaac";
    vector<int> neededTime{1, 2, 3, 4, 5};
    int expected = 3;

    ASSERT_EQ(Solution::minCost(colors, neededTime), expected);

}

TEST(C1578_MinimumTimeColorful, NoRemoval) {
    string colors = "abc";
    vector<int> neededTime{1, 2, 3};
    int expected = 0;

    ASSERT_EQ(Solution::minCost(colors, neededTime), expected);

}


TEST(C1578_MinimumTimeColorful, Multiremoval) {
    string colors = "aabaa";
    vector<int> neededTime{1, 2, 3, 4, 1};
    int expected = 2;

    ASSERT_EQ(Solution::minCost(colors, neededTime), expected);

}

