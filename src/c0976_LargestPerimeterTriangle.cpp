//
// Created by anton on 12/10/2022.
//
//
// Given an integer array nums, return the largest perimeter of a triangle with a non-zero area,
// formed from three of these lengths. If it is impossible to form any triangle of a non-zero area, return 0.
//



#include "solution.h"
#include <algorithm>

int Solution::largestPerimeter(vector<int> &nums) {
//    We will search for the longest values a < b < c such as c > b + a
    sort(nums.begin(), nums.end());
    for (int i = (int) nums.size() - 1; i >= 2; i--) {
        if (nums[i] < nums[i - 1] + nums[i - 2]) {
            return nums[i] + nums[i - 1] + nums[i - 2];
        }
    }
    return 0;
}