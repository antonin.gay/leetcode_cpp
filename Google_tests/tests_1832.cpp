//
// Created by anton on 17/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"


TEST(C1832_SentencePangram, trueSentence) {
    string sentence = "thequickbrownfoxjumpsoverthelazydog";
    bool expected = true;

    ASSERT_EQ(Solution::checkIfPangram(sentence), expected);
}

TEST(C1832_SentencePangram, falseSentence) {
    string sentence = "leetcode";
    bool expected = false;

    ASSERT_EQ(Solution::checkIfPangram(sentence), expected);
}

