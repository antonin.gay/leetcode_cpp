//
// Created by anton on 14/10/2022.
//


#include "solution.h"

//todo: implement tests using utils functions
int deleteMiddleRecursive(ListNode *node, int dist_from_begin) {
    int dist_from_end;
    if (node->next == nullptr) {
        dist_from_end = 0;
    } else {
        dist_from_end = deleteMiddleRecursive(node->next, dist_from_begin + 1);
    }

//    We are the middle node if abs(dist_from_end - dist_from_begin) <= 1.
//    Therefore, we are at the previous node if abs((dist_from_end+1) - (dist_from_begin-1)) <= 1
//    Therefore we remove the current node
    if (dist_from_end - dist_from_begin == 1 or dist_from_end - dist_from_begin == 2) {
        ListNode *removed_node = node->next;
        node->next = node->next->next;
        delete removed_node;
    }

//    We return the distance from end of the previous node, i.e. current + 1
    return dist_from_end + 1;
}

ListNode *Solution::deleteMiddle(ListNode *head) {
    if (head->next == nullptr) {
        return nullptr;
    }

    ListNode *slow = head;
    ListNode *fast = head->next->next;

//    At each loop, the slow ptr walks one, fast walks two.
//    Therefore, when fast.next or fast.n.next is null, slow is just before the middle.
    while (fast != nullptr and fast->next != nullptr) {
        slow = slow->next;
        fast = fast->next->next;
    }
    slow->next = slow->next->next;

    return head;
}