//
// Created by anton on 29/09/2022.
//

#include "gtest/gtest.h"
#include "solution.h"


TEST(C0658_KClosestElements, Example01) {
//    Init
    vector<int> arr{1, 2, 3, 4, 5};
    int k = 4;
    int x = 3;
    const vector<int> expected{1, 2, 3, 4};

//    Running
    const vector<int> result = Solution::findClosestElements(arr, k, x);

//    Testing
    EXPECT_EQ(result.size(), expected.size()) << "Lists are of unequal length";
    for (int i = 0; i < expected.size(); i++) {
        EXPECT_EQ(result[i], expected[i]) << "Vectors x and y differ at index " << i;
    }
}

TEST(C0658_KClosestElements, Example02) {
//    Init
    vector<int> arr{1, 2, 3, 4, 5};
    int k = 4;
    int x = -1;
    const vector<int> expected{1, 2, 3, 4};

//    Running
    const vector<int> result = Solution::findClosestElements(arr, k, x);

//    Testing
    EXPECT_EQ(result.size(), expected.size()) << "Lists are of unequal length";
    for (int i = 0; i < expected.size(); i++) {
        EXPECT_EQ(result[i], expected[i]) << "Vectors x and y differ at index " << i;
    }
}