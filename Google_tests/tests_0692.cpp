//
// Created by anton on 19/10/2022.
//

#include "gtest/gtest.h"
#include "solution.h"

TEST(Challenge_0692, ExampleOne) {
    vector<string> words{"i", "love", "leetcode", "i", "love", "coding"};
    int k = 2;
    vector<string> expected{"i", "love"};

    ASSERT_EQ(Solution::topKFrequent(words, k), expected);
}

TEST(Challenge_0692, ExampleTwo) {
    vector<string> words{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"};
    int k = 4;
    vector<string> expected{"the", "is", "sunny", "day"};

    ASSERT_EQ(Solution::topKFrequent(words, k), expected);
}
