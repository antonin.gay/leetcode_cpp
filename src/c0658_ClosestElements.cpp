//
// Created by anton on 29/09/2022.
//

#include <algorithm>
#include "solution.h"

vector<int> Solution::findClosestElements(vector<int> &arr, int k, int x) {
    std::sort(arr.begin(), arr.end(),
              [&x](int a, int b) {
                  return abs(x - a) < abs(x - b) or (abs(x - a) == abs(x - b) and a < b);
              });

    std::sort(&arr[0], &arr[k]);
    return {&arr[0], &arr[k]};
}