//
// Created by anton on 02/10/2022.
//

#include "solution.h"
#include <cmath>

int get_results_recursive(int n, int k, int target, int **results) {
    if (target < n or target > n * k) {
        return 0;
    }
//    if (target == n){
//        return 1;
//    }
//  If there is only one die and target between 1 and k, we return 1
    if (n == 1) {
        return 1;
    }

//  If already computed, we return directly the value
    if (results[n][target] != -1) {
        return results[n][target];
    }

//    Else we compute recursively
    int sum = 0;
    for (int i = 1; i < min(k + 1, target); i++) {
        sum += get_results_recursive(n - 1, k, target - i, results);
        sum = sum % int(pow(10, 9) + 7);
    }
    results[n][target] = sum;
    return sum;
}

int Solution::numRollsToTarget(int n, int k, int target) {
    int *results[n + 1];
    for (int i = 0; i < (n + 1); i++) {
        results[i] = new int[target + 1];
        std::fill_n(results[i], target + 1, -1);
    }

    return get_results_recursive(n, k, target, results);
}
