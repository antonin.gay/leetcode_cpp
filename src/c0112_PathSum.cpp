//
// Created by anton on 04/10/2022.
//

#include "solution.h"

//todo: could be optimised by being stopped when a branch reaches true
//todo: develop utils function to create tree in order to implement tests
bool Solution::hasPathSum(TreeNode *root, int targetSum) {
    if (root == nullptr) {
//        If the given node is a nullptr, we are out of a branch and return false
        return false;
    } else if (root->left == nullptr and root->right == nullptr) {
//        If the current node is a leaf (left==null & right==null) we return true if val==target else false
        return root->val == targetSum;
    } else {
//        Otherwise we are at the middle of a branch, and we return if one of the below branch is true
        return Solution::hasPathSum(root->left, targetSum - root->val) or
               Solution::hasPathSum(root->right, targetSum - root->val);
    }
}