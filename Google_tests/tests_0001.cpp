//
// Created by anton on 27/09/2022.
//

#include "gtest/gtest.h"
#include "solution.h"


TEST(C0001_TwoSumTestSuite, ExampleSum) {
    vector<int> nums{2, 7, 11, 15};
    vector<int> res{0, 1};
    ASSERT_EQ(Solution::twoSum(nums, 9), res);
}

